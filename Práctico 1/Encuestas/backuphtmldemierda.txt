<div class="row" style="width: 50%; margin: 0 auto; text-align: center">
    <div class="col-md-12" style="width: 600px">
        <h3 class="text-center"> Datos Personales </h3>
        <form role="form">
            <div class="form-group">
                <label for="dni"> DNI </label>
                <input type="text" class="form-control" id="dni" name="dni">
            </div>
            <div class="form-group">
                <label for="nombre"> Nombre </label>
                <input type="text" class="form-control" id="nombre" name="nombre">
            </div>
            <div class="form-group">
                <label for="apellido"> Apellido </label>
                <input type="text" class="form-control" id="apellido" name="apellido">
            </div>
            <div class="form-group">
                <label for="direccion"> Direccion </label>
                <input type="text" class="form-control" id="direccion" name="direccion">
            </div>
            <div class="form-group">
                <label for="telefono"> Telefono </label>
                <input type="text" class="form-control" id="telefono" name="telefono">
            </div>
            <div class="form-group">
                <label for="localidad"> Localidad </label>
                <input type="text" class="form-control" id="localidad" name="localidad">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1"> Email </label>
                <input type="email" class="form-control" id="email" name="email">
            </div>
        </form>
    </div>
</div>
<div class="row" style="width: 50%; margin: 0 auto; text-align: center">
    <div class="col-md-12">
        <h3 class="text-center"> Encuesta </h3>
        <div class="row">
            <p> Con qué partido político simpatiza más? </p>
            <select title="simpatiza">
                <option value="">-----</option>
                <option value="fpv">Frente Para la Victoria</option>
                <option value="pro">PRO (Cambiemos)</option>
                <option value="fdi">Frente de Izquierda</option>
            </select>
        </div>
        <div class="row">
            <p> A su criterio, qué opcion caracterizaría la situación actual del país? </p>
            <select title="situacion">
                <option value="">-----</option>
                <option value="direccion-correcta">En la dirección correcta</option>
                <option value="direccion-estatica">No se mueve</option>
                <option value="direccion-equivocada">En la dirección contraria</option>
            </select>
        </div>
        <div class="row">
            <p> En qué area considera que el gobierno debería poner más foco? </p>
            <select title="foco">
                <option value="">-----</option>
                <option value="educacion">Educación</option>
                <option value="salud">Salud</option>
                <option value="empleo">Empleo</option>
                <option value="seguridad">Seguridad</option>
            </select>
        </div>
        <div class="row">
            <p> Qué tan satisfecho se encuentra con el modelo económico actual? </p>
            <select title="eco">
                <option value="">-----</option>
                <option value="eco-insatisfecho">Insatisfecho</option>
                <option value="eco-satisfecho">Satisfecho</option>
                <option value="eco-muysatisfecho">Muy satisfecho</option>
            </select>
        </div>
        <div class="row">
            <p> Qué tan satisfecho se encuentra con las posibilidades laborales? </p>
            <select title="labo">
                <option value="">-----</option>
                <option value="labo-insatisfecho">Insatisfecho</option>
                <option value="labo-satisfecho">Satisfecho</option>
                <option value="labo-muysatisfecho">Muy satisfecho</option>
            </select>
        </div>
        <div class="row">
            <p> Qué tan satisfecho se encuentra con el sistema de salud? </p>
            <select title="salud">
                <option value="">-----</option>
                <option value="sal-insatisfecho">Insatisfecho</option>
                <option value="sal-satisfecho">Satisfecho</option>
                <option value="sal-muysatisfecho">Muy satisfecho</option>
            </select>
        </div>
        <div class="row">
            <p> Qué tan satisfecho se encuentra con el sistema educativo? </p>
            <select title="edu">
                <option value="">-----</option>
                <option value="edu-insatisfecho">Insatisfecho</option>
                <option value="edu-satisfecho">Satisfecho</option>
                <option value="edu-muysatisfecho">Muy satisfecho</option>
            </select>
        </div>
    </div>
</div>
