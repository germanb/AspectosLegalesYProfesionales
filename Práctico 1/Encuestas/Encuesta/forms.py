from django import forms
from .models import Encuesta, Persona


OPCIONES_PARTIDO = [
    ("FpV", "Frente para la Victoria"),
    ("Pro", "Cambiemos"),
    ("FI", "Frente de Izquierda")
]
OPCIONES_SITUACION = [
    ("dir_1", "En la dirección correcta"),
    ("dir_2", "Estático"),
    ("dir_3", "En la dirección contraria")
]
OPCIONES_FOCO = [
    ("edu", "Educación"),
    ("sal", "Salud"),
    ("emp", "Empleo"),
    ("seg", "Seguridad")
]
OPCIONES_ECO = [
    ("ins", "Insatisfecho"),
    ("sat", "Satisfecho"),
    ("msa", "Muy Satisfecho")
]
OPCIONES_LABO = [
    ("ins", "Insatisfecho"),
    ("sat", "Satisfecho"),
    ("msa", "Muy Satisfecho")
]
OPCIONES_SALUD = [
    ("ins", "Insatisfecho"),
    ("sat", "Satisfecho"),
    ("msa", "Muy Satisfecho")
]
OPCIONES_EDU = [
    ("ins", "Insatisfecho"),
    ("sat", "Satisfecho"),
    ("msa", "Muy Satisfecho")
]


class EncuestaForm(forms.ModelForm):
    partido = forms.ChoiceField(choices=OPCIONES_PARTIDO, label="Partido Político al que pertenece")
    partido.initial = '-----'
    situacion = forms.ChoiceField(choices=OPCIONES_SITUACION, label="Opinión sobre la situación general")
    foco = forms.ChoiceField(choices=OPCIONES_FOCO, label="Donde debería hacer foco el gobierno")
    mod_eco = forms.ChoiceField(choices=OPCIONES_ECO, label="Opinión sobre el Modelo Económico")
    mod_labo = forms.ChoiceField(choices=OPCIONES_LABO, label="Opinión sobre el Modelo Laboral")
    mod_salud = forms.ChoiceField(choices=OPCIONES_SALUD, label="Opinión sobre el Modelo Salud")
    mod_edu = forms.ChoiceField(choices=OPCIONES_EDU, label="Opinión sobre el Modelo Educativo")

    def __init__(self, *args, **kwargs):
        super(EncuestaForm, self).__init__(*args, *kwargs)

    def save(self, commit=True):
        e = super(EncuestaForm, self).save()

        e = Encuesta()
        e.ideologia = self.cleaned_data['partido']
        e.conformidad = self.cleaned_data['situacion']
        e.foco = self.cleaned_data['foco']
        e.educacion = self.cleaned_data['mod_edu']
        e.salud = self.cleaned_data['mod_salud']
        e.trabajo = self.cleaned_data['mod_labo']
        e.economia = self.cleaned_data['mod_eco']
        e.save()
        print(e)

        p = Persona()
        p.dni = self.cleaned_data['dni']
        p.nombre = self.cleaned_data['nombre']
        p.apellido = self.cleaned_data['apellido']
        p.direccion = self.cleaned_data['direccion']
        p.telefono = self.cleaned_data['telefono']
        p.localidad = self.cleaned_data['localidad']
        p.email = self.cleaned_data['email']
        print(p)

        p.encuesta = e
        e.persona = p

        e.save()
        p.save()

        if commit:
            e.save()
            p.save()

        return p

    class Meta:
        model = Persona
        fields = ("dni",
                  "nombre",
                  "apellido",
                  "direccion",
                  "telefono",
                  "localidad",
                  "email")
