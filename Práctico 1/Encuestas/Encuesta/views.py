from .forms import EncuestaForm
from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Encuesta, Persona

def index(request):
    if request.method == "POST":
        form = EncuestaForm(request.POST)
        if form.is_valid():
            form.save()
    else:
        form = EncuestaForm()
    return render(request, 'index.html', {'form': form})
