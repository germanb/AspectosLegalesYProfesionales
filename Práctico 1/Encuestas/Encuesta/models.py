from django.db import models


class Encuesta(models.Model):
    ideologia = models.CharField(max_length=100, null=False, default="")
    conformidad = models.CharField(max_length=100, null=False, default="")
    foco = models.CharField(max_length=100, null=False, default="")
    educacion = models.CharField(max_length=100, null=False, default="")
    salud = models.CharField(max_length=100, null=False, default="")
    trabajo = models.CharField(max_length=100, null=False, default="")
    economia = models.CharField(max_length=100, null=False, default="")


class Persona(models.Model):
    dni = models.IntegerField(primary_key=True, null=False)
    nombre = models.CharField(max_length=100, null=False)
    apellido = models.CharField(max_length=100, null=False)
    direccion = models.CharField(max_length=100, null=True)
    telefono = models.IntegerField(null=True)
    localidad = models.CharField(max_length=100, null=True)
    email = models.EmailField(max_length=100, default='test@test.com')
    encuesta = models.OneToOneField(Encuesta, null=True, blank=True)

    def __str__(self):
        return "{} {}".format(self.nombre, self.apellido)
